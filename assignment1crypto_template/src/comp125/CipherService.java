//Dylan Westbury
//43329896

package comp125;
import java.util.Random;

public class CipherService {
	
	/**
	 * @param s
	 * @return input string after removing leading spaces
	 */
	public static String removeLeadingSpaces(String s) {
		char[] charArray = s.toCharArray();
		int count = 0;
		
		//Count leading whitespaces
		for (int i = 0; i < s.length(); i++)
			if (charArray[i] == ' ')
				count++;
			else
				break;
		
		//Remove whitespaces
		s = s.substring((count));
		return s;
	}

	/**
	 * @param s
	 * @return input string after removing trailing spaces
	 */
	public static String removeTrailingSpaces(String s) {
		char[] charArray = s.toCharArray();
		int count = 0;
		
		//Count trailing spaces
		for (int i = s.length() - 1; i >= 0; i--)
			if (charArray[i] == ' ')
				count++;
			else
				break;
		//Remove whitespaces
		s = s.substring(0, s.length() - count);
		return s;
	}
	
	/**
	 * @param s
	 * @return input string after replacing all consecutive spaces by a single space
	 */
	public static String removeMultipleSpaces(String s) {
		//Use regular expression to replace reduce whitespaces
		s = s.replaceAll("\\s+", " ");
		
		return s;
	}

	/**
	 * 
	 * @param s
	 * @return string in the simplified form, that is without leading or trailing spaces and with single space separated "words"
	 */
	public static String simplify(String s) {
		s = removeLeadingSpaces(s);
		s = removeTrailingSpaces(s);
		s = removeMultipleSpaces(s);
		return s;
	}
	
	public static char[] setMapping() {
		char [] mappings = new char[63];
		//a-z
		for (int i=0; i<26 ; i++)
			mappings[i] = (char)(97+i);
		//A-Z
		for (int i=0; i<26; i++)
			mappings[i+26] = (char)(65+i);
		//0-9
		for (int i=0; i<=9; i++)
			mappings[i+52] = Character.forDigit(i, 10);
		//Space ' '
		mappings[62] = ' ';
		
		return mappings;
	}
	
	/**
	 * @return one to one mapping of a set containing alphanumeric characters (and space) to itself
	 */
	public static char[] generateMapping() {
		char [] mappings = setMapping();
		
		int randIndex = 0;
		int i = mappings.length - 1;
		Random r = new Random();
		char c = ' ';
		
		//Shuffle organised mappings
		do {
			randIndex = r.nextInt(mappings.length);
			c = mappings[i];
			mappings[i] = mappings[randIndex];
			mappings[randIndex] = c;
		 	i--;
		} while (i > 0);
			
		return mappings; //to be completed
	}

	/**
	 * @param mapping
	 */
	public static void displayMapping(char[] mapping) {
		/*
		 * first print 
		 * lower case alphabets, followed by
		 * upper case alphabets, followed by
		 * digits, followed by
		 * space
		 */
		
		for(int i=0; i<mapping.length; i++) {
			if(i < 26)
				System.out.println((char)('a'+i)+" --- "+mapping[i]);
			else if(i < 52)
				System.out.println((char)('A'+i-26)+" --- "+mapping[i]);
			else
				System.out.println((char)('0'+i-52)+" --- "+mapping[i]);
		}
	}
	
	/**
	 * @param mapping
	 * @param ch
	 * @return index at which character exists in mapping
	 */
	public static int indexOf(char[] mapping, char ch) {
		int result = 0;
		
		for (int i=0; i < mapping.length; i++)
			if (ch == mapping[i])
				result = i;
		
		return result;
	}

	/**
	 * @param plaintext
	 * @param mapping
	 * @return plaintext encrypted using the alphanumeric (and space) mapping
	 * simplify plaintext before encrypting
	 */
	public static String encrypt(String plaintext, char[] mapping) {
		String text = simplify(plaintext);
		String encrypted = "";
		int beforeIndex = 0;
		
		char [] charArr = text.toCharArray();
		char [] before = setMapping();
		char [] after = mapping;
		
		for (int i=0; i < charArr.length; i++) {
			beforeIndex = indexOf(before, charArr[i]);
			encrypted += after[beforeIndex];		
		}
		
		return encrypted;
	}

	/**
	 * @param ciphertext
	 * @param mapping
	 * @return ciphertext decrypted with alphanumeric (and space) mapping
	 */
	public static String decrypt(String ciphertext, char[] mapping) {
		String text = simplify(ciphertext);
		String decrypted = "";
		int beforeIndex = 0;
		
		char [] charArr = text.toCharArray();
		char [] before = setMapping();
		char [] after = mapping;
		
		for (int i=0; i < charArr.length; i++) {
			beforeIndex = indexOf(after, charArr[i]);
			decrypted += before[beforeIndex];		
		}

		return decrypted;
	}

	/**
	 * 
	 * a client to carry out a demonstration of our functionalities
	 * @param args
	 */
	public static void main(String[] args) {

		char[] mapping = generateMapping();
		
		System.out.println("mapping:\t");
		displayMapping(mapping);

		String plaintext = "Transfer 109bn into my NAB account";

		System.out.println("message:\t"+plaintext);

		String ciphertext = encrypt(plaintext, mapping);

		System.out.println("cipher:\t\t"+ciphertext);

		String decrypted = decrypt(ciphertext, mapping);

		System.out.println("decrypted:\t"+decrypted);

		plaintext = "   Transfer        109bn    into   my   NAB   account       ";

		System.out.println("message (raw):\t\t"+plaintext);

		plaintext = simplify(plaintext);
		
		System.out.println("message (simplified):\t"+plaintext);

		ciphertext = encrypt(plaintext, mapping);

		System.out.println("cipher:\t\t\t"+ciphertext);

		decrypted = decrypt(ciphertext, mapping);

		System.out.println("decrypted: "+decrypted);

	}
}
