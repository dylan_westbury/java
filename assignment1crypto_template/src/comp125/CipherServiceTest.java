package comp125;

import static org.junit.Assert.*;

import org.junit.Test;

public class CipherServiceTest {
	@Test
	public void testGenerateMapping() {
		int size = 1000;

		String[] mappings = new String[size];
		for(int i=0; i<mappings.length; i++)
			mappings[i] = "";

		int falseCount = 0;
		
		for(int i=0; i<size; i++) {
			char[] current = CipherService.generateMapping(); 
			
			assertEquals(63, current.length);
			
			for(char ch = 'a'; ch <= 'z'; ch++)
				assertTrue(String.valueOf(current).indexOf(ch) >= 0);

			for(char ch = 'A'; ch <= 'Z'; ch++)
				assertTrue(String.valueOf(current).indexOf(ch) >= 0);

			for(char ch = '0'; ch <= '9'; ch++)
				assertTrue(String.valueOf(current).indexOf(ch) >= 0);

			assertTrue(String.valueOf(current).indexOf(' ') >= 0);
			
			String s = new String(current);
			if(contains(mappings, s))
				falseCount++;
			else
				mappings[i] = s;
		}
		assertTrue(falseCount < 3);
	}

	private boolean contains(String[] a, String s) {
		for(int i=0; i<a.length; i++)
			if(a[i].equalsIgnoreCase(s))
				return true;
		return false;
	}

	@Test
	public void testEncrypt() {
		assertEquals("uS G5", CipherService.encrypt("abcde", 		 	  "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("cDP6", CipherService.encrypt("0123",   		 	  "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("uS EvBDP6", CipherService.encrypt("abcABC123", 	  "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("uS FEvBFDP6", CipherService.encrypt("abc ABC 123",  "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("", CipherService.encrypt("", 						  "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("uS FEvBFDP6", CipherService.encrypt("          abc       ABC       123         ", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));		
	}

	@Test
	public void testDecrypt() {
		assertEquals("abcde", CipherService.decrypt("uS G5", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("0123", CipherService.decrypt("cDP6", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("abcABC123", CipherService.decrypt("uS EvBDP6", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("abc ABC 123", CipherService.decrypt("uS FEvBFDP6", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
		assertEquals("", CipherService.decrypt("", "uS G5MdghrQbIN48TtLYUV19moEvBJzZk2AKRxyaXwl0ei7n3pHCcDP6WfOsjqF".toCharArray()));
	}

	@Test
	public void testRemoveTrailingSpaces() {
		assertEquals("", CipherService.removeTrailingSpaces(""));
		assertEquals("", CipherService.removeTrailingSpaces("      "));
		assertEquals("h", CipherService.removeTrailingSpaces("h     "));
		assertEquals("hello", CipherService.removeTrailingSpaces("hello"));
		assertEquals("hello", CipherService.removeTrailingSpaces("hello "));
		assertEquals("   hello", CipherService.removeTrailingSpaces("   hello   "));
	}

	@Test
	public void testRemoveLeadingSpaces() {
		assertEquals("", CipherService.removeLeadingSpaces(""));
		assertEquals("", CipherService.removeLeadingSpaces("      "));
		assertEquals("h", CipherService.removeLeadingSpaces("     h"));
		assertEquals("hello", CipherService.removeLeadingSpaces("hello"));
		assertEquals("hello", CipherService.removeLeadingSpaces(" hello"));
		assertEquals("hello   ", CipherService.removeLeadingSpaces("   hello   "));
	}

	@Test
	public void testRemoveMultipleSpaces() {
		assertEquals("", CipherService.removeMultipleSpaces(""));
		assertEquals(" ", CipherService.removeMultipleSpaces("    "));
		assertEquals("hello", CipherService.removeMultipleSpaces("hello"));
		assertEquals(" hello there, everyone ", CipherService.removeMultipleSpaces("   hello     there,      everyone     "));
	}

	@Test
	public void testSimplify() {
		assertEquals("", CipherService.simplify(""));
		assertEquals("", CipherService.simplify("    "));
		assertEquals("hello", CipherService.simplify("hello"));
		assertEquals("hello", CipherService.simplify("hello       "));
		assertEquals("hello", CipherService.simplify("       hello"));
		assertEquals("hello", CipherService.simplify("       hello     "));
		assertEquals("hello there, everyone", CipherService.simplify("   hello     there,      everyone     "));
	}
}
